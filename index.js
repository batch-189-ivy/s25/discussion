//alert(`Hello`)


/*
	JSON Objects
		JSON stands for Javascript Object Notation

		JSON is also used in other programming language hence the name JavaScript Object Notation

	Syntax:
		{
			"propertyA": "valueA"
			"propertyB": "valueB"
		}

*/

//JSON Object
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

//JSON Array

/*"cities" = [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Manila", "province": "Metro Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]
*/
//JSON Methods
	//The JSON Object contains methods for parsing and converting data into stringified JSON

//Converting Data into a stringified JSON

let batchesArr = [
	{batchName: "Batch 189"},
	{batchName: "Batch 190"}
]

console.log(batchesArr)

//The stringify method is used to convert JS objects into a string
//Beofre sending data, convert array or an object to its string equivalent
//stringify:
console.log("Result from a stringify method: ");
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city:"Manila",
		country: "Philippines"
	}
})

console.log(data)


//User Details
/*let firstName = prompt("What is your first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong?")
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData);
*/

//Convert stringified JSON into JS Objects
	//Json.parse()


let batchesJSON = `[
	{"batchName": "Batch 189"},
	{"batchName": "Batch 190"}
]`

console.log(batchesJSON)


// Upon receiving data, the JSON text can be converted to JS object so that we can use it in our program
console.log("Result from parse method: ")
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `{
	"name": "Ivy",
	"age": "18",
	"address": {
		"city": "Caloocan City",
		"country": "Philippines"
	}
}`

console.log(stringifiedObject);
console.log(JSON.parse(stringifiedObject));

